# This is a git repository with website pictures loaded at the top-sites button.

## How to use:
1. click png file
1. copy URL: for example: https://gitlab.cern.ch/weiy/firefox_website_pictures/-/blob/master/outlook.png
1. change url as online view mode by adding **?inline=true** at the end of the url and change **blob** to **raw**: https://gitlab.cern.ch/weiy/firefox_website_pictures/-/raw/master/outlook.png?inline=true 
1. copy the above link and paste it into ``Custom Image URL``
1. restart firefox
